FROM registry.gitlab.com/thallian/docker-confd-env:master

RUN apk add --no-cache monit

RUN mkdir /etc/monit/
RUN mkdir /monitoring

ADD /rootfs /

EXPOSE 2812
