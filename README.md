Monitor stuff with [Monit](https://mmonit.com/monit/).

# Volumes
- `/etc/monit/conf.d`: place your monit configuration files in here 

+ everything you want to monitor.

# Environment Variables
## MONIT_INTERVAL
- default: 120

Monitoring interval in seconds.

## MONIT_WEB_USER
User for the monit web interface.

## MONIT_WEB_PASSWORD
Password for the monit web interface.

# Ports
- 2812
